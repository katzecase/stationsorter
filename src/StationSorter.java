import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


public class StationSorter {
    @Test
    public void testMachine(){
        assertArrayEquals(new int[]{2, 1}, sortAndCheckDuplication(new int[]{2, 1}));
        assertArrayEquals(new int[]{1, 3, 2}, sortAndCheckDuplication(new int[]{1, 3, 2}));
        assertArrayEquals(new int[]{1,1,2}, sortAndCheckDuplication(new int[]{1, 1, 2}));
        assertArrayEquals(new int[]{1,2,4}, sortAndCheckDuplication(new int[]{1,2,4}));
        assertArrayEquals(new int[]{1,2,5,7,5,2,5}, sortAndCheckDuplication(new int[]{1,2,5,7,5,2,5}));
        assertArrayEquals(new int[]{6,5,0,7,0,4,5}, sortAndCheckDuplication(new int[]{6,5,0,7,0,4,5}));
    }

    @Test
    public void testNoCont() {
        assertTrue(checkNoCont(sort(new int[]{2, 1})));
        assertTrue(checkNoCont(sort(new int[]{1, 1, 2})));
        assertTrue(checkNoCont(sort(new int[]{1, 2, 5})));
        assertTrue(checkNoCont(sort(new int[]{1, 2, 5, 7, 5, 2, 5})));
        assertTrue(checkNoCont(sort(new int[]{6,5,0,7,0,4,5})));
    }

    private static boolean checkNoCont(int[] sortedArr){
        for(int i = 0; i < sortedArr.length - 1; i++)
            if(sortedArr[i] == sortedArr[i+1])
                return false;
        return true;
    }

    public int[] sortAndCheckDuplication(int [] arr){
        return checkDuplication(sort(arr));
    }

    /*
    duplication checks
     */
//    @Test
    public void testCanMatch(){
        assertArrayEquals(new int[]{2, 1}, checkDuplication(new int[]{1, 2, 1}));
        assertArrayEquals(new int[]{3, 1}, checkDuplication(new int[]{1, 2, 1, 1}));
        assertArrayEquals(new int[]{3, 2, 4}, checkDuplication(new int[]{1, 2, 1, 3, 1, 3, 3, 2, 3}));
        assertArrayEquals(new int[]{0, 2, 1}, checkDuplication(new int[]{2, 3, 2}));
        assertArrayEquals(new int[]{0, 2, 0, 1}, checkDuplication(new int[]{2, 4, 2}));
        assertArrayEquals(new int[]{1,2,5}, checkDuplication(new int[]{3, 2, 3, 1, 3, 2, 3, 3}));
    }

    private static List<Integer> duplicateArr;

    private static  int[] checkDuplication(int[] inputs){
        List<Integer> inputList = new ArrayList<>();
        for(int i: inputs)
            inputList.add(i);

        duplicateArr = new ArrayList<>();
        calculateDuplication(inputList, 1);

        System.out.println("check duplication: " + duplicateArr.toString());
        return duplicateArr.stream().mapToInt(i->i).toArray();
    }

    private static void calculateDuplication(List<Integer> inputList, int listIndex){
        int count = 0;
        for(int i = 0; i < inputList.size(); i++)
            if(inputList.get(i) == listIndex){
                count++;
                inputList.remove(i--);
            }

        duplicateArr.add(count);

        if(inputList.size() > 0)
            calculateDuplication(inputList, ++listIndex);
    }


    private static int[] sort(int[] arr) {
        int sum = 0;
        for(int i: arr)
            sum += i;
        int[] output = new int[sum];

        for(int i = 0; i < sum; i++)
            if(i > 0 && output[i-1] == maxAndSecIndex(arr)[0] + 1){
                output[i] = maxAndSecIndex(arr)[1] + 1;
                arr[maxAndSecIndex(arr)[1]]--;
            } else{
                output[i] = maxAndSecIndex(arr)[0] + 1;
                arr[maxAndSecIndex(arr)[0]]--;
            }

        System.out.print("sorted array: ");
        for(int a: output)
           System.out.print(a + ", ");
        System.out.println();
        return output;
    }


//    @Test
    public void testMaxPos(){
        assertEquals(0, maxAndSecIndex(new int[] {2})[0]);
        assertEquals(0, maxAndSecIndex(new int[] {2, 1})[0]);
        assertEquals(1, maxAndSecIndex(new int[] {1, 3})[0]);
        assertEquals(2, maxAndSecIndex(new int[] {1, 3, 4})[0]);
        assertEquals(3, maxAndSecIndex(new int[] {1, 5, 4, 7, 2})[0]);
        assertEquals(1, maxAndSecIndex(new int[] {1, 3, 4})[1]);
        assertEquals(1, maxAndSecIndex(new int[] {1, 5, 4, 7, 2})[1]);
        assertEquals(0, maxAndSecIndex(new int[] {6, 4, 4, 7, 2})[1]);
        assertEquals(3, maxAndSecIndex(new int[] {4, 1, 1, 2})[1]);
        assertEquals(1, maxAndSecIndex(new int[] {4, 4, 3, 2})[1]);
        assertEquals(1, maxAndSecIndex(new int[] {7, 4, 3, 2})[1]);
        assertEquals(0, maxAndSecIndex(new int[] {7, 4, 3, 2})[0]);
        assertEquals(2, maxAndSecIndex(new int[] {7, 4, 5, 2})[1]);
        assertEquals(2, maxAndSecIndex(new int[] {7, 4, 5, 2})[1]);
    }

    private static int[] maxAndSecIndex(int[] arr) {
        int max = 0;
        int sec = 1;
        for(int i = 1; i < arr.length; i++)
            if (arr[i] > arr[max]) {
                sec = max;
                max = i;
            }else if (arr[i] > arr[sec])
                sec = i;

        return new int[] {max, sec};
    }

}
